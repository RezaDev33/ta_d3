## Smart Parking System (Tugas Akhir)

Aplikasi ini dapat melacak posisi kendaran dengan mencocokan capture camera di tempat parkir dengan dengan gambar mobil yang didapat dari pintu masuk, kemudian melakukan tracking untuk mengatahui tempat parkir mobil tersebut.

### Tentang Aplikasi

- Dibuat dengan mengunakan framework Django dan OpenCv 
- Dapat mengetahui karakter pada plat nomor dengan Tecerract OCR
- Dapat melakukan tracking pada mobil yang akan parkir di lahan parkir

### App Preview
![Landing Page](/Assets/landing_page.png "Landing Page")
![Denah Parkir](/Assets/denah.png "Denah Parkir")
![Dashboard](/Assets/home.png "Dashboard")
