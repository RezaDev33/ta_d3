from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse ,JsonResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required

from .formTitik import PosisiForm
from .models import Posisi
from mobil.models import Kendaraan
from paket.reza import ImageProses
from json import dumps
import os
import base64
# Create your views here.

@login_required
def titik(request):
    posisi = Posisi.objects.all()
    return render(request, 'parkir/titik/titik.html', {'posisi':posisi})

@login_required
def imgTitik(request):
    image = ImageProses()
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__)) # File direktori
    image.setImage(PROJECT_ROOT+"/static/mobil/parkiran.jpg")
    image.setSize((400,300))
    posisi = Posisi.objects.all()
    img = image.get_imageTitikParkir(posisi)
    return HttpResponse(img)

@login_required
def editGambar(request):
    data = dict()
    image = ImageProses()
    template_name = 'parkir/titik/ganti_gambar.html'
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    image.setImage(PROJECT_ROOT+"/static/mobil/parkiran.jpg")
    image.setDenah(PROJECT_ROOT+"/static/mobil/denah.png")
    image.setSize((400,300))
    parkiran = image.get_image()
    denah = image.get_denah()
    context = {'parkiran': parkiran, 'denah':denah}
    if request.method == 'POST':
        try:
            gambar_parkiran = request.POST.get("b64parkiran")
            gambar_denah = request.POST.get("b64denah")
            _, imgparkiran = gambar_parkiran.split(';base64,')
            _, imgdenah = gambar_denah.split(';base64,')
            with open(PROJECT_ROOT+"/static/mobil/parkiran.jpg", "wb") as fh:
                fh.write(base64.b64decode(imgparkiran))
            with open(PROJECT_ROOT+"/static/mobil/denah.png", "wb") as fh:
                fh.write(base64.b64decode(imgdenah))
            data['form_is_valid'] = True
            posisi = Posisi.objects.all()
            data['html_titik_list'] = render_to_string('parkir/titik/titik_list.html',{
                'posisi':posisi
            })
        except:
            data['form_is_valid'] = False
            data['html_form'] = render_to_string(template_name, context, request=request)
    else:
        data['form_is_valid'] = False
        data['html_form'] = render_to_string(template_name,context, request=request)
    return JsonResponse(data)

@login_required
def save_posisi_form(request, form, context, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            posisi = Posisi.objects.all()
            data['html_titik_list'] = render_to_string('parkir/titik/titik_list.html',{
                'posisi':posisi
            })
        else:
            data['form_is_valid'] = False
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
def createPosisi(request):
    if request.method == 'POST':
        form = PosisiForm(request.POST)
    else:
        form = PosisiForm()
    image = ImageProses()
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__)) # File direktori
    image.setImage(PROJECT_ROOT+"/static/mobil/parkiran.jpg")
    image.setDenah(PROJECT_ROOT+"/static/mobil/denah.png")
    image.setSize((400,300))
    parkiran = image.get_image()
    denah = image.get_denah()
    context = {'form': form, 'parkiran':parkiran, 'denah':denah}
    return save_posisi_form(request, form, context, 'parkir/titik/posisi_create.html')

@login_required
def editPosisi(request,pk):
    posisi = get_object_or_404(Posisi, pk=pk)
    if request.method == 'POST':
        form = PosisiForm(request.POST, instance=posisi)
    else:
        form = PosisiForm(instance=posisi)
    image = ImageProses()
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__)) # File direktori
    image.setImage(PROJECT_ROOT+"/static/mobil/parkiran.jpg")
    image.setDenah(PROJECT_ROOT+"/static/mobil/denah.png")
    image.setSize((400,300))
    parkiran = image.get_imageTitikParkirSebelum(posisi.koordinat, warna = (255,0,0))
    denah = image.get_denahTitikParkirSebelum(posisi.koordinat_denah, warna = (255,0,0))
    context = {'form': form, 'parkiran':parkiran, 'denah':denah}
    return save_posisi_form(request, form, context, 'parkir/titik/posisi_update.html')

@login_required
def deletePosisi(request, pk):
    posisi = get_object_or_404(Posisi, pk=pk)
    data = dict()
    if request.method == 'POST':
        posisi.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        posisi = Posisi.objects.all()
        data['html_titik_list'] = render_to_string('parkir/titik/titik_list.html',{
            'posisi':posisi
        })
    else:
        context = {'posisi': posisi}
        data['html_form'] = render_to_string('parkir/titik/posisi_delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)

@login_required
def cari_kendaraan_template(request):
    return render(request, 'parkir/cari_kendaraan/cari_mobil.html')

def cari_kendaraan(request):
    data = dict()
    image = ImageProses()
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__)) # File direktori
    image.setImage(PROJECT_ROOT+"/static/mobil/denah.png")
    image.setSize((400,300))
    if request.method == 'POST':
        plat = request.POST.get("plat")
        cek_mobil = Kendaraan.objects.filter(plat_nomor = plat,status_parkir=True,status_kendaraan=True)
        if len(cek_mobil) == 1:
            mobil = Posisi.objects.raw('SELECT * FROM parkir_posisi LEFT JOIN parkir_parkir ON parkir_posisi.id = parkir_parkir.id_posisi_id LEFT JOIN mobil_kendaraan ON parkir_parkir.id_kendaraan_id = mobil_kendaraan.id WHERE mobil_kendaraan.plat_nomor = "'+plat+'"')[0]
            img = image.get_imageCariKendaraan(mobil,(0,0,255))
            data['cek_in'] = mobil.cek_in
            data['cek_mobil'] = True
        else:
            img = image.get_image()
            data['cek_mobil'] = False
    else:
        img = image.get_image()
    data['img'] = img
    return JsonResponse(data)

@login_required
def tempat_kosong_template(request):
    return render(request, 'parkir/tempat_kosong/tempat_kosong.html')

def img_tempat_kosong(request):
    data = dict()
    image = ImageProses()
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__)) # File direktori
    image.setImage(PROJECT_ROOT+"/static/mobil/denah.png")
    image.setSize((400,300))
    data_parkir = Posisi.objects.raw('SELECT parkir_posisi.nama, parkir_posisi.koordinat_denah, parkir_parkir.id FROM parkir_posisi LEFT JOIN parkir_parkir ON parkir_posisi.id = parkir_parkir.id_posisi_id')    
    img, slot = image.get_imageParkirKosong(data_parkir)
    data['img'] = img
    data['slot'] = slot
    return JsonResponse(data)