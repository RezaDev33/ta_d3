from django.db import models

# Create your models here.

class Posisi(models.Model):
    nama = models.CharField(max_length=200)
    koordinat = models.CharField(max_length=200)
    koordinat_denah = models.CharField(max_length=200, default="")
    
class Parkir(models.Model):
    id_kendaraan = models.ForeignKey('mobil.Kendaraan',on_delete=models.CASCADE)
    id_posisi = models.ForeignKey(Posisi,on_delete=models.CASCADE)
    gambar_parkir = models.CharField(max_length=200 ,default="", editable=False)