from django.urls import path

from . import views

app_name = 'parkir'
urlpatterns = [
    path('titik/', views.titik, name='titik'),
    path('titik/img/',views.imgTitik, name='image_titik'),
    path('titik/img/ganti/', views.editGambar, name='ganti_gambar'),
    path('titik/create/',views.createPosisi, name='create_posisi'),
    path('titik/<int:pk>/edit/', views.editPosisi, name='edit_posisi'),
    path('titik/<int:pk>/delete/', views.deletePosisi, name='delete_posisi'),
    path('cari_mobil/', views.cari_kendaraan_template, name='cari_mobil'), # template
    path('cari_kendaraan/', views.cari_kendaraan, name='cari_kendaraan'), # cari kendaraan
    path('parkir_kosong/',views.tempat_kosong_template, name='parkir_kosong'), # template
    path('img_tempat_kosong/', views.img_tempat_kosong, name='img_tempat_kosong'), # cari tempat kosong
] 