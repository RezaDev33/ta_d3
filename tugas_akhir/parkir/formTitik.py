from django import forms
from .models import Posisi

class PosisiForm(forms.ModelForm):
    class Meta:
        model = Posisi
        fields = ('nama','koordinat','koordinat_denah')
