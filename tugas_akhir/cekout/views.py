from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required

from paket.reza import ImageProses
from .models import Tarif
from mobil.models import Kendaraan
from parkir.models import Posisi, Parkir
from datetime import datetime
from .formsTarif import TarifForm

import os
# Create your views here.
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
mobil_keluar = ""

@login_required
def index(request):
    return render(request, 'cekout/index.html')

@login_required
def aturHarga(request):
    harga = get_object_or_404(Tarif, pk=1)
    if request.method == 'POST':
        form = TarifForm(request.POST, instance=harga)
    else:
        form = TarifForm(instance=harga)
    return save_tarif_form(request, form, 'cekout/tarif.html')

@login_required
def save_tarif_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
def image(request):
    global PROJECT_ROOT,mobil_keluar
    data = dict()
    image = ImageProses()
    if request.method == 'POST':
        plat = request.POST.get("plat")
        mobil = Kendaraan.objects.filter(plat_nomor=plat,status_kendaraan=True)
        if len(mobil) == 1:
            data_mobil = mobil[0]
            data_cek_in = data_mobil.cek_in.replace(" ",".").replace(":",".").split(".")
            image.setImage(data_mobil.gambar_ori)
            mobil_keluar = plat

            last_year = int(data_cek_in[2])
            last_moon = int(data_cek_in[1])
            last_day = int(data_cek_in[0])
            last_hour = int(data_cek_in[3])
            last_minute = int(data_cek_in[4])
            today = datetime.now()
            last_date = datetime(last_year,last_moon,last_day,last_hour,last_minute)
            selisih = today-last_date
            duration_in_s = selisih.total_seconds()

            harga = Tarif.objects.get(id=1)
            tarif_dasar = harga.harga_dasar
            tarif_penalty = harga.harga_penalty
            hours = divmod(duration_in_s, 3600)
            tarif_dasar = tarif_dasar * int(hours[0])
            if int(hours[0]) == 0 and int(hours[1] >= 0):
                tarif_dasar += tarif_penalty
            if int(hours[1]) >= 1800:
                tarif_dasar += tarif_penalty
            date = today.strftime("%d.%m.%Y %H:%M")
            data['cek_in'] = data_mobil.cek_in
            data['cek_out'] = date 
            data['tarif'] = tarif_dasar
            data['data_is_valid'] = True
            data['cek_mobil'] = True
        else:
            data['cek_mobil'] = False
    else:
        data['data_is_valid'] = False
    image.setSize((600,400))
    img = image.get_image()
    data['img'] = img
    return JsonResponse(data)

@login_required
def parkirSelesai(request):
    global PROJECT_ROOT, mobil_keluar
    data = dict()
    plat = request.POST.get("plat")
    if mobil_keluar == plat:
        today = datetime.now()
        date = today.strftime("%d.%m.%Y %H:%M")
        mobil = Kendaraan.objects.filter(plat_nomor=plat,status_kendaraan=True).update(cek_out=date,status_kendaraan = False)
        data['plat'] = plat
        data['cek_mobil'] = True
    else:
        data['cek_mobil'] = False
    return JsonResponse(data)
