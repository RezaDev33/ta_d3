from django import forms
from .models import Tarif

class TarifForm(forms.ModelForm):
    class Meta:
        model = Tarif
        fields = ('harga_dasar', 'harga_penalty', )