from django.urls import path

from . import views

app_name = 'cekout'
urlpatterns = [
    path('', views.index, name='index'),
    path('img/', views.image, name='image'),
    path('harga/', views.aturHarga, name='atur_harga'),
    path('selesai/', views.parkirSelesai, name='selesai'),
] 