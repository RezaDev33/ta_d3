from django.db import models

# Create your models here.
class Tarif(models.Model):
    harga_dasar = models.BigIntegerField()
    harga_penalty = models.BigIntegerField()