import cv2
import numpy as np
import imutils
import math
import datetime

from parkir.models import Parkir, Posisi
from mobil.models import Kendaraan
import base64
import os
import urllib.request
import sys

from pytesseract import image_to_string
from .parkir import Mobil, Image, Titik

def prosesMobil(mask_roi,sift,flann,gambar,yc,xc,hc,wc,xt,yt):
    data_mobil = Mobil()
    roi = mask_roi[yc:yc+hc,xc:xc+wc]
    data_mobil.setKoordinat((xt,yt))
    kp_sift_roi, des_sift_roi = sift.detectAndCompute(roi,None)
    # print(len(kp_sift_roi))
    hasil_akhir = []
    for i in range(0,len(gambar)):
        matches = flann.knnMatch(des_sift_roi,gambar[i].getDescriptoin(),k=2)
        good = []
        for m,n in matches:
            if m.distance < 0.8*n.distance:
                good.append(m)
        presen = round(len(good)/len(gambar[i].getKeypoint()) * 100,2)
        hasil_akhir.append(presen)
    print(hasil_akhir)
    hasil_terbaik = hasil_akhir.index(max(hasil_akhir))
    # print(hasil_terbaik)
    if hasil_akhir[hasil_terbaik] > 10:
        data_mobil.setDetec(True)
        data_mobil.setPlat(gambar[hasil_terbaik].getPlat())
        data_mobil.setId(gambar[hasil_terbaik].getId())
    return data_mobil

def ttk_tengah(x, y, w, h):
    x1 = int(w / 2)
    y1 = int(h / 2)
    cx = x + x1
    cy = y + y1
    return cx,cy

def terbesar(x,y):
    if x > y:
        hasil = x-y
        return hasil
    else:
        hasil = y-x
        return hasil

def split(arr, size):
     arrs = []
     while len(arr) > size:
         pice = arr[:size]
         arrs.append(pice)
         arr   = arr[size:]
     arrs.append(arr)
     return arrs

# class untuk prosesing video di tempat parkir
class VideoProses(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)
        self.resize = (400,300)
        self.sift = cv2.xfeatures2d.SIFT_create()
        self.subtraction = cv2.bgsegm.createBackgroundSubtractorMOG()
        self.flann = cv2.FlannBasedMatcher(dict(algorithm = 0, trees = 5), dict(checks = 50))
        self.min_width = 39
        self.min_height = 39
        self.min_width_gerbang = 250
        self.min_height_gerbang = 250
        self.mobil = []
        self.gambar = []
        self.titik = []
        self.mobil_parkir = ""
        self.cari_mobil = False #tadi di pasang
        self.ulang = False
        self.garis_y = 140
        self.offset = 5
        self.roi = []

    def __del__(self):
        self.video.release()

    def setVideo(self,file): # 1. inisialisasi video
        self.video = cv2.VideoCapture(file)
        if self.video.isOpened():
            return True
        else:
            return False

    def setSize(self,size):
        self.resize = size

    def setTitikParkir(self,titik):
        for data in titik:
            size_frame = self.resize
            data_koordinat = tuple(data.koordinat.split(','))
            x = int(size_frame[0] * int(data_koordinat[0]) / 400) # perbandingan untuk resize x, 400 adalah width di fitur titik
            y = int(size_frame[1] * int(data_koordinat[1]) / 300) # perbandingan untuk resize y, 300 adalah height di fitur titik
            data_titik = Titik(data.id,data.nama,(x,y))
            if data.plat_nomor is not None: # jika udah ada mobil yang parkir
                data_mobil = Mobil()
                data_mobil.setKoordinat((x,y))
                data_mobil.setDetec(True)
                data_mobil.setPlat(data.plat_nomor)
                data_mobil.setIdTitik(data.id)
                self.mobil.append(data_mobil) # masukan data tersebut ke kumpulan objek mobil
                data_titik.setIsi(True) # buat titik tersebut sudah terisi
            self.titik.append(data_titik)

    def setGambarMobil(self,mobil):
        sift_method = self.sift
        for data in mobil:
            grey = cv2.imread(data.gambar,0)
            grey = cv2.resize(grey,self.resize) # resize biar sama
            kp_sift, des_sift = sift_method.detectAndCompute(grey,None)
            # print(len(kp_sift))
            data_gambar = Image(data.id,data.plat_nomor,kp_sift,des_sift)
            self.gambar.append(data_gambar)         

    def get_frame(self):
        ret,image = self.video.read()
        if ret:
            image_mini = cv2.resize(image,self.resize)
            asli,hasil = self.trackingMobil(image_mini,image,self.mobil,self.gambar,self.titik)
            # hasil = cv2.resize(hasil,self.resize)
            # asli = cv2.resize(asli,self.resize)
            ret,asli_jpeg = cv2.imencode('.jpg',asli)
            ret,hasil_jpeg = cv2.imencode('.jpg',hasil)
            img_asli = asli_jpeg.tobytes()
            img_hasil = hasil_jpeg.tobytes()
        else:
            img_asli = self.get_frameFail()
            img_hasil = self.get_frameFail()
        return img_asli, img_hasil

    def get_frameGerbangMasuk(self):
        ret,image = self.video.read()
        if ret:
            image_mini = cv2.resize(image,self.resize)
            asli,hasil = self.gerbangMasuk(image_mini,image)
            # hasil = cv2.resize(hasil,self.resize)
            # asli = cv2.resize(asli,self.resize)
            ret,asli_jpeg = cv2.imencode('.jpg',asli)
            ret,hasil_jpeg = cv2.imencode('.jpg',hasil)
            img_asli = asli_jpeg.tobytes()
            img_hasil = hasil_jpeg.tobytes()
        else:
            img_asli = self.get_frameFail()
            img_hasil = self.get_frameFail()
        return img_asli, img_hasil

    def get_frameParkir(self): # tidak digunakan 
        ret,image = self.video.read()
        if ret:
            image_mini = cv2.resize(image,self.resize)
            asli,hasil = self.trackingMobil(image_mini,image,self.mobil,self.gambar,self.titik)
            asli,hasil = self.gerbangMasuk(image_mini,image,self.mobil,self.gambar,self.titik)
            ret,asli_jpeg = cv2.imencode('.jpg',asli)
            ret,hasil_jpeg = cv2.imencode('.jpg',hasil)
            img_asli = asli_jpeg.tobytes()
            img_hasil = hasil_jpeg.tobytes()
        else:
            img_asli = self.get_frameFail()
            img_hasil = self.get_frameFail()
        return img_asli, img_hasil

    def gerbangMasuk(self,frame,image_asli):
        frame_roi = frame.copy()
        grey = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(grey,(3,3),5)
        img_sub = self.subtraction.apply(grey)
        dilat = cv2.dilate(img_sub,np.ones((5,5)))
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (20, 20))
        dilation = cv2.morphologyEx (dilat, cv2.MORPH_CLOSE , kernel)
        dilation = cv2.morphologyEx (dilation, cv2.MORPH_CLOSE , kernel)

        _,contours,hirarki = cv2.findContours(dilation,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted(contours, key = cv2.contourArea, reverse = True)[:10]
        out = np.zeros_like(grey)
        try: 
            cv2.fillPoly(out, pts =[cnts[0]], color=(255,255,255)) # cnts[0] adalah contur terbesar
        except:
            pass

        # seleksi gambar asli dengan hasil BGS
        mask_roi = cv2.bitwise_and(frame,frame,mask=out)

        cv2.line(frame, (25, self.garis_y), (550, self.garis_y), (255,127,0), 3) 
        (x,y,w,h) = cv2.boundingRect(out)
        
        validar_contorno = (w >= self.min_width_gerbang) and (h >= self.min_height_gerbang)
        
        if validar_contorno:
            center = ttk_tengah(x, y, w, h)
            cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)        
            cv2.circle(frame, (center[0],center[1]), 4,(0, 0,255), -1)
            if center[1]<(self.garis_y+self.offset) and center[1]>(self.garis_y-self.offset) and x < 600:
                self.ulang = True # terdeteksi
                cv2.line(frame, (25, self.garis_y), (550, self.garis_y), (0,127,255), 3)  
                self.roi = mask_roi
            elif self.ulang == True: # ambil frame terakhir untuk di lakukan ektraksi fitur
                #untuk plat
                PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
                date_string = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
                img_gerbang_bgs = PROJECT_ROOT+"/src/img/mobil-"+date_string+".jpg"
                img_gerbang_asli = PROJECT_ROOT+"/src/img/mobil_ori-"+date_string+".jpg"
                #untuk cekin
                today = datetime.datetime.today()
                date = today.strftime("%d.%m.%Y %H:%M")
                #save data
                mobil_masuk = Kendaraan(plat_nomor = "Belum di data", cek_in = date, status_kendaraan = True,status_parkir = False, gambar=img_gerbang_bgs,gambar_ori=img_gerbang_asli)# date string untuk sementara
                mobil_masuk.save()
                cv2.imwrite(img_gerbang_bgs,self.roi)
                cv2.imwrite(img_gerbang_asli,image_asli)
                self.ulang = False # hapus deteksi        
        return image_asli, frame

    def trackingMobil(self,frame,frame_asli,mobil,gambar,titik):
        # proses pengolahan citra pada frame
        cp_frame = frame.copy()
        grey = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY) # mengubah frame menjadi gray scale
        blur = cv2.GaussianBlur(grey,(3,3),5)
        img_sub = self.subtraction.apply(blur)
        out = np.zeros_like(grey) # membuat matrik 0 dengan memiliki ukuran yang sama dengan frame
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (20, 20))
        dilation = cv2.morphologyEx (img_sub, cv2.MORPH_CLOSE , kernel)
        dilation = cv2.morphologyEx (dilation, cv2.MORPH_CLOSE , kernel)

        for i in range(0,len(titik)):
            posisi_titik = titik[i].getKoordinat()
            cv2.circle(frame,(posisi_titik),3,(0,0,255),cv2.FILLED) # mengambar titik parkir
            if titik[i].getIsi() == False:
                cv2.putText(frame,titik[i].getNama(),(posisi_titik[0],posisi_titik[1]-4), cv2.FONT_HERSHEY_SIMPLEX,0.5, [0,0,255])

        _,contours,hirarki = cv2.findContours(dilation,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) # langsung external counture
        
        cv2.fillPoly(out, pts = contours, color=(255,255,255))

        mask_roi = cv2.bitwise_and(cp_frame,cp_frame,mask=dilation)

        if gambar == []: # jika list kosong atau data mobil kosong
            return frame,frame
        else:
            for i in range(0,len(contours)):
                (xc,yc,wc,hc) = cv2.boundingRect(contours[i])
                if (wc >= self.min_width) and (hc >= self.min_height) == True:
                    (xt,yt) = ttk_tengah(xc, yc, wc, hc)
                    cv2.circle(frame, (xt,yt), 4, (0, 0,255), -1)
                    
                    if len(mobil) == 0:
                        data_mobil = prosesMobil(mask_roi,self.sift,self.flann,gambar,yc,xc,hc,wc,xt,yt)
                        mobil.append(data_mobil)
                    
                    self.cari_mobil = False
                    for j in range(0,len(mobil)):
                        koordinat_mobil = mobil[j].getKoordinat()
                        if abs(koordinat_mobil[0] - xt) <= 20 and abs(koordinat_mobil[1] - yt) <= 20:
                            plat = mobil[j].getPlat()
                            mobil[j].setKoordinat((xt,yt))
                            cv2.rectangle(frame,(xc,yc),(xc+wc,yc+hc),(0,255,0),2)
                            cv2.putText(frame,plat,(xc,yc-5), cv2.FONT_HERSHEY_SIMPLEX,0.5, [0,255,0])
                            self.cari_mobil = True
                                
                    if self.cari_mobil == False:
                        data_mobil = prosesMobil(mask_roi,self.sift,self.flann,gambar,yc,xc,hc,wc,xt,yt)
                        mobil.append(data_mobil)

            jarak = []
            for i in range(0,len(titik)):
                data_titik = titik[i].getKoordinat()
                # print(len(mobil))
                for j in range(0,len(mobil)):
                    if mobil[j].getKoordinat() != ():
                        koordinat_mobil = mobil[j].getKoordinat()
                        koordinat_titik = titik[i].getKoordinat()
                        data1 = terbesar(koordinat_mobil[0],koordinat_titik[0])
                        data2 = terbesar(koordinat_mobil[1],koordinat_titik[1])
                        jarak_titik = math.sqrt((data1)^2 + (data2)^2)
                        jarak.append(jarak_titik)
                        # print(jarak)
                    if out[data_titik[1],data_titik[0]] == 255 and titik[i].getIsi() == False:
                        # print(jarak)
                        if mobil[j].getId() == -1:
                            continue
                        else:
                            mobil_terdekat = jarak.index(min(jarak))
                            mobil[j].setIdTitik(mobil_terdekat)
                            titik[i].setIsi(True)
                            PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
                            date_string = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
                            img_parkir = PROJECT_ROOT+"/src/img/mobil-parkir-"+date_string+".jpg"
                            cv2.imwrite(img_parkir,frame_asli)
                            id_mobil = Kendaraan.objects.get(id = mobil[j].getId())
                            Kendaraan.objects.filter(id = mobil[j].getId()).update(status_parkir=True)
                            id_titik = Posisi.objects.get(id = titik[i].getId())
                            parkir = Parkir(id_kendaraan = id_mobil, id_posisi = id_titik, gambar_parkir=img_parkir)
                            parkir.save()
                    if titik[i].getIsi() == True and mobil[j].getIdTitik() != -1:
                        cv2.circle(frame,(data_titik),5,(0,255,0),cv2.FILLED)
                        cv2.putText(frame, titik[i].getNama()+" Terisi oleh "+mobil[j].getPlat(),(data_titik[0]+6,data_titik[1]), cv2.FONT_HERSHEY_SIMPLEX,0.5, [0,255,0],2)
    
            return cp_frame,frame

    def get_frameFail(self):
        PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
        frame = cv2.imread(PROJECT_ROOT+"/no_camera.jpg") # file
        frame = cv2.resize(frame,self.resize)
        ret, frame_buff = cv2.imencode('.jpg', frame)
        return frame_buff.tobytes()

    def getTitikParkir(self):
        return self.titik

    def getGambarMobil(self):
        return self.gambar

class IpCamera(VideoProses):
    def __init__(self):
        super().__init__()
        self.url = ""

    def __del__(self):
        cv2.destroyAllWindows()

    def getUrl(self):
        return self.url
        
    def setUrl(self,url):
        try:
            imgResp = urllib.request.urlopen(url)
            self.url = url
            return True
        except:
            return False

    def setUrlRstp(self,file):
        os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp" #untuk realtime
        self.video = cv2.VideoCapture(file,cv2.CAP_FFMPEG) # untuk realtime
        if self.video.isOpened():
            return True
        else:
            return False

    def get_frame(self): # untuk yg lewat rstp
        ret,image = self.video.read()
        if ret:
            image_mini = cv2.resize(image,self.resize)
            asli,hasil = super().trackingMobil(image_mini,image,self.mobil,self.gambar,self.titik)
            # hasil = cv2.resize(hasil,self.resize)
            # asli = cv2.resize(asli,self.resize)
            ret,asli_jpeg = cv2.imencode('.jpg',asli)
            ret,hasil_jpeg = cv2.imencode('.jpg',hasil)
            img_asli = asli_jpeg.tobytes()
            img_hasil = hasil_jpeg.tobytes()
        else:
            img_asli = super().get_frameFail()
            img_hasil = super().get_frameFail()
        return img_asli, img_hasil

    def get_frame_http(self): # untuk yg konek lewat http
        # ambil image
        try:
            imgResp = urllib.request.urlopen(self.url)
            imgNp = np.array(bytearray(imgResp.read()),dtype=np.uint8)
            img = cv2.imdecode(imgNp,-1)

            img_mini = cv2.resize(img,self.resize)
            asli,hasil = super().trackingMobil(img_mini,img,self.mobil,self.gambar,self.titik)

            ret,jpeg_asli = cv2.imencode('.jpg',asli)
            ret,jpeg_hasil = cv2.imencode('.jpg',hasil)
            img_asli = jpeg_asli.tobytes()
            img_hasil = jpeg_hasil.tobytes()
        except:
            img_asli = super().get_frameFail()
            img_hasil = super().get_frameFail()
        return img_asli, img_hasil


class ImageProses(object):
    def __init__(self):
        PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
        self.image = cv2.imread(PROJECT_ROOT+"/No-image-found.jpg")
        self.denah = cv2.imread(PROJECT_ROOT+"/No-image-found.jpg")
        self.resize = (400,300)

    def encodeImage(self,img,format_img='.jpg'):
        ret, image = cv2.imencode(format_img, img) # encode image
        image = base64.b64encode(image).decode('ascii') # encode mengunakan base 64 dengan decode('ascii')
        return image

    def setImage(self,img):
        self.image = cv2.imread(img)

    def setDenah(self,img):
        self.denah = cv2.imread(img)

    def setSize(self,size):
        self.resize = size

    def get_image(self,grayScale=False):
        img = self.image
        if (grayScale == True):
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.resize(img,self.resize)
        image = self.encodeImage(img)
        return image

    def get_denah(self,grayScale=False):
        img = self.denah
        if (grayScale == True):
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.resize(img,self.resize)
        image = self.encodeImage(img,format_img='.png')
        return image

    def get_imageTitikParkir(self,titik,warna=(0,0,255)):
        img = self.image
        img = cv2.resize(img,self.resize)
        for point in titik:
            res = tuple(point.koordinat.split(','))
            cv2.circle(img,(int(res[0]),int(res[1])),5,warna,-1)
            cv2.putText(img,point.nama, (int(res[0]),int(res[1])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, warna)
        image = self.encodeImage(img)
        return image
        
    def get_imageCariKendaraan(self,posisi,warna=(0,0,255)):
        img = self.image
        img = cv2.resize(img,self.resize)
        res = tuple(posisi.koordinat_denah.split(','))
        cv2.circle(img,(int(res[0]),int(res[1])),5,warna,-1)
        cv2.putText(img,posisi.nama, (int(res[0]),int(res[1])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, warna)
        image = self.encodeImage(img)
        return image

    def get_imageParkirKosong(self,posisi,warna_p = (0,0,225), warna_k = (0,255,0)):
        img = self.image
        img = cv2.resize(img,self.resize)
        kosong = 0
        for data in posisi:
            res = tuple(data.koordinat_denah.split(','))
            if data.id == None:
                cv2.circle(img,(int(res[0]),int(res[1])),5,warna_k,-1)
                cv2.putText(img,data.nama, (int(res[0]),int(res[1])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, warna_k)
                kosong += 1
            else:
                cv2.circle(img,(int(res[0]),int(res[1])),5,warna_p,-1)
                cv2.putText(img,data.nama, (int(res[0]),int(res[1])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, warna_p)
        image = self.encodeImage(img)
        return image, kosong
        
    def get_imageTitikParkirSebelum(self,titik,warna=(0,0,255)):
        img = self.image
        img = cv2.resize(img,self.resize)
        res = tuple(titik.split(','))
        cv2.circle(img,(int(res[0]),int(res[1])),5,warna,-1)
        cv2.putText(img,"Titik Sebelum", (int(res[0]),int(res[1])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, warna)
        image = self.encodeImage(img)
        return image

    def get_denahTitikParkirSebelum(self,titik,warna=(0,0,255)):
        img = self.denah
        img = cv2.resize(img,self.resize)
        res = tuple(titik.split(','))
        cv2.circle(img,(int(res[0]),int(res[1])),5,warna,-1)
        cv2.putText(img,"Titik Sebelum", (int(res[0]),int(res[1])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, warna)
        image = self.encodeImage(img)
        return image

    def get_sift(self,imgAwal):
        PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
        img = self.image
        img = cv2.resize(img,self.resize)
        firstImg = cv2.imread(imgAwal)
        firstImg = cv2.resize(firstImg,self.resize)

        diff = cv2.absdiff(firstImg,img)
        grey = cv2.cvtColor(diff,cv2.COLOR_BGR2GRAY)
        res,diff_th = cv2.threshold(grey,20,225,cv2.THRESH_BINARY)
            
        dilat = cv2.dilate(diff_th,np.ones((5,5)))
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        closing = cv2.morphologyEx (dilat, cv2.MORPH_CLOSE , kernel)
        erosi = cv2.erode(closing,kernel,iterations=1)
        
        _,contours,hirarki = cv2.findContours(closing,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted(contours, key = cv2.contourArea, reverse = True)[:10]
        out = np.zeros_like(grey)
        hasil = np.zeros_like(img)

        cv2.fillPoly(out, pts =[cnts[0]], color=(255,255,255)) 
        hasil = cv2.bitwise_and(img,img,mask=out)

        date_string = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
        img_name = PROJECT_ROOT+"/src/img/mobil-"+date_string+".jpg"
        img_ori = PROJECT_ROOT+"/src/img/mobil_ori-"+date_string+".jpg"
        cv2.imwrite(img_name,hasil)
        cv2.imwrite(img_ori,img)

        return img_ori, img_name

    def get_sift_paraller(self,imgAwal,imgAsli):
        PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
        img = self.image
        img = cv2.resize(img,self.resize)
        firstImg = cv2.imread(imgAwal)
        firstImg = cv2.resize(firstImg,self.resize)

        asliImg = cv2.imread(imgAsli)
        asliImg = cv2.resize(asliImg,self.resize)

        diff = cv2.absdiff(firstImg,img)
        grey = cv2.cvtColor(diff,cv2.COLOR_BGR2GRAY)
        res,diff_th = cv2.threshold(grey,100,225,cv2.THRESH_BINARY)
            
        dilat = cv2.dilate(diff_th,np.ones((5,5)))
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (8, 8))
        closing = cv2.morphologyEx (dilat, cv2.MORPH_CLOSE , kernel)
        # erosi = cv2.erode(closing,kernel,iterations=1)
        
        _,contours,hirarki = cv2.findContours(closing,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted(contours, key = cv2.contourArea, reverse = True)[:10]
        out = np.zeros_like(grey)
        hasil = np.zeros_like(img)

        cv2.fillPoly(out, pts =[cnts[0]], color=(255,255,255)) 
        hasil = cv2.bitwise_and(img,img,mask=out)

        date_string = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
        img_name = PROJECT_ROOT+"/src/img/mobil-"+date_string+".jpg"
        img_ori = PROJECT_ROOT+"/src/img/mobil_ori-"+date_string+".jpg"
        cv2.imwrite(img_name,hasil)
        cv2.imwrite(img_ori,asliImg)

        return img_ori, img_name


    def get_ocrPlat(self,x,y,width,height):
        img = self.image
        img = cv2.resize(img,self.resize)
        roi = img[y: y+height, x: x+width]

        karakter_hmax = int(height/2)
        karakter_wmax = int(width/3) 
        vol_karakter = int(width*height/50)

        sharpen_kernel = np.array([[-1,-1,-1], [-1,11,-1], [-1,-1,-1]])
        sharpen = cv2.filter2D(roi, -1, sharpen_kernel)
        g_image = cv2.cvtColor(sharpen, cv2.COLOR_BGR2GRAY)
        ret3,th3 = cv2.threshold(g_image,130,255,cv2.THRESH_BINARY_INV)
        _, contours, hierarchy = cv2.findContours(th3,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        area = sorted(contours, key=cv2.contourArea, reverse=True)
        cp_grey = np.zeros_like(g_image)

        for i in area:
            x_roi,y_roi,w_roi,h_roi = cv2.boundingRect(i)
            vol = w_roi*h_roi
            if (w_roi <= karakter_wmax and h_roi <= karakter_hmax):
                if(vol >= vol_karakter):
                    cv2.fillPoly(cp_grey, pts =[i], color=(255,255,255)) 
                else:
                    cv2.fillPoly(cp_grey, pts =[i], color=(0,0,0)) 
        
        hasil = cv2.bitwise_and(g_image,g_image,mask=cp_grey)
        ret4,th4 = cv2.threshold(hasil,50,255,cv2.THRESH_BINARY_INV)

        config = ('-l eng --oem 1 --psm 3')

        text =  image_to_string(th4, config=config)
        return text

