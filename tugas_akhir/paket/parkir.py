class Mobil:
    __noMobil = 0
    def __init__(self):
        Mobil.__noMobil += 1
        self.__no = Mobil.__noMobil
        self.__id = -1
        self.__id_titik = -1
        self.__plat = "Tidak ketahui"
        self.__koordinat = () # koordinat center 
        self.__size = () # bounding box
        self.__parkir = False
        self.__detec = False

    def getNo(self):
        return self.__no

    def getId(self):
        return self.__id

    def getIdTitik(self):
        return self.__id_titik

    def getPlat(self):
        return self.__plat

    def getParkir(self):
        return self.__parkir
    
    def getDetec(self):
        return self.__detec

    def getKoordinat(self):
        return self.__koordinat

    def getSize(self):
        return self.__size

    def setId(self,id):
        self.__id = id

    def setIdTitik(self,id):
        self.__id_titik = id

    def setPlat(self,plat):
        self.__plat = plat

    def setSize(self,size):
        self.__size = size

    def setKoordinat(self,koordinat):
        self.__koordinat = koordinat

    def setParkir(self,parkir):
        self.__parkir = parkir

    def setDetec(self,detec):
        self.__detec = detec

class Titik:
    def __init__(self,idtitik,nama,koordinat):
        self.__id = idtitik
        self.__nama = nama
        self.__koordinat = koordinat
        self.__terisi = False
    
    def getId(self):
        return self.__id

    def getNama(self):
        return self.__nama

    def getKoordinat(self):
        return self.__koordinat

    def getIsi(self):
        return self.__terisi

    def setIsi(self,isi):
        self.__terisi = isi

class Image:
    def __init__(self,idgambar,plat,kp,des):
        self.__id = idgambar
        self.__plat = plat
        self.__kp = kp
        self.__des = des

    def getId(self):
        return self.__id

    def getPlat(self):
        return self.__plat

    def getKeypoint(self):
        return self.__kp

    def getDescriptoin(self):
        return self.__des
