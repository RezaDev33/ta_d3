from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse,StreamingHttpResponse,HttpResponseServerError,JsonResponse
from django.views.decorators import gzip
from django.template.loader import render_to_string
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import login_required

from mobil.models import Kendaraan
from dashboard.models import Camera
from paket.reza import ImageProses, VideoProses, IpCamera
from dashboard.formsCamera import CameraForm

from datetime import datetime
import os
import base64

# Create your views here.
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

def gen_gerbangmasuk(camera,status): # generate video gerbang masuk
    camera.setSize((600,400))
    if status == True:
        while True:
            frame, frame_track = camera.get_frameGerbangMasuk()
            # frame, frame_track = camera.get_frame_rstp()
            yield(b'--frame\r\n'
            b'Content-Type: image/jpeg\r\n\r\n' + frame_track + b'\r\n\r\n')            
    else:
        frame = camera.get_frameFail()
        yield(b'--frame\r\n'
        b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@gzip.gzip_page
def videoGerbangMasuk(request):
    global PROJECT_ROOT
    kamera = Camera.objects.get(id=2)# id 2 untuk gerbang masuk
    if kamera.status_active == 0:
        vid = VideoProses() 
        cam = vid.setVideo(PROJECT_ROOT+kamera.file_camera)
        # print(cam)
    elif kamera.status_active == 1:
        vid = VideoProses()
        cam = vid.setVideo(kamera.web_camera)
    else:
        vid = IpCamera()
        # cam = vid.setUrl(kamera.ip_camera)
        cam = vid.setUrlRstp(kamera.ip_camera)
    try:
        return StreamingHttpResponse(gen_gerbangmasuk(vid,cam),content_type="multipart/x-mixed-replace;boundary=frame")
    except HttpResponseServerError as e:
        return HttpResponse(e)

@login_required
def aturCamera(request):
    cam = get_object_or_404(Camera, pk=2)
    if request.method == 'POST':
        form = CameraForm(request.POST, instance=cam)
    else:
        form = CameraForm(instance=cam)
    return save_camera_form(request, form, 'cekin/camera.html')

@login_required
def save_camera_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
def index(request):
    kendaraan = Kendaraan.objects.filter(plat_nomor="Belum di data").order_by('-id')
    return render(request, 'cekin/index.html',{'kendaraan':kendaraan})

@login_required
def imgGerbang(request):
    global PROJECT_ROOT
    data = dict()
    image = ImageProses()
    if request.method == 'POST':
        try:
            gambar_awal = request.POST.get("b64awal")
            gambar_mobil = request.POST.get("b64mobil")
            _, imgawal = gambar_awal.split(';base64,')
            _, imgmobil = gambar_mobil.split(';base64,')
            with open(PROJECT_ROOT+"/static/image/imageAwal.jpg", "wb") as fh:
                fh.write(base64.b64decode(imgawal))
            with open(PROJECT_ROOT+"/static/image/imageMobil.jpg", "wb") as fh:
                fh.write(base64.b64decode(imgmobil))
            image.setImage(PROJECT_ROOT+"/static/image/imageMobil.jpg")
            data['form_is_valid'] = True
        except:
            data['html_form'] = render_to_string('cekin/tambah_gambar.html', request=request)
            data['form_is_valid'] = False
    else:
        data['form_is_valid'] = False
    image.setSize((600,400))
    img = image.get_image()
    data['img'] = img
    return JsonResponse(data)

@login_required
def setImage(request):
    global PROJECT_ROOT
    data = dict()
    image = ImageProses()
    if request.method == 'GET':
        kendaraan = Kendaraan.objects.filter(plat_nomor="Belum di data").order_by('-id')
        print(kendaraan)
        if kendaraan:
            print("sas")
            image.setImage(kendaraan[0].gambar_ori)
            data['id_img'] = kendaraan[0].id
            image.setSize((600,400))
            img = image.get_image()
            data['img'] = img
            data['html_list'] = render_to_string('cekin/list.html',{
                'kendaraan':kendaraan
            })
            data['form_is_valid'] = True
        else:
            image.setSize((600,400))
            img = image.get_image()
            data['img'] = img
            data['id_img'] = 0
            data['form_is_valid'] = False
    else:
        data['form_is_valid'] = False
    return JsonResponse(data)

@login_required
def deteksiImage(request):
    global PROJECT_ROOT
    data = dict()
    image = ImageProses()
    x = int(request.POST.get("x"))
    y = int(request.POST.get("y"))
    w = int(request.POST.get("width"))
    h = int(request.POST.get("height"))
    image.setImage(PROJECT_ROOT+"/static/image/imageMobil.jpg") 
    image.setSize((600,400))
    text = image.get_ocrPlat(x,y,w,h)
    data['text'] = text
    return JsonResponse(data)

@login_required
def simpanImage(request):
    global PROJECT_ROOT
    data = dict()
    text = request.POST.get("text")
    id_mobil = request.POST.get("id")
    print(id_mobil)
    cek_mobil = Kendaraan.objects.filter(plat_nomor = text)
    if len(cek_mobil)==0:
        data['text'] = text
        mobil = Kendaraan.objects.get(id=id_mobil)
        mobil.plat_nomor = text
        mobil.save()
        data['cek_mobil'] = True
    else:
        data['cek_mobil'] = False        
    kendaraan = Kendaraan.objects.filter(plat_nomor="Belum di data").order_by('-id')
    return JsonResponse(data)

@login_required
def lihatGambar(request,pk):
    data = dict()
    image = ImageProses()
    mobil = get_object_or_404(Kendaraan, pk=pk)
    image.setImage(mobil.gambar_ori)
    gambar_mobil = image.get_image()
    data['form_is_valid'] = True
    data['html_kendaraan_list'] = render_to_string('cekin/lihat_gambar.html', {
                'gambar_mobil': gambar_mobil
            })
    context = {'gambar_mobil':gambar_mobil}
    data['html_form'] = render_to_string('cekin/lihat_gambar.html', context, request=request)
    return JsonResponse(data)

# hanya template form
@login_required
def tambahImage(request): 
    data = dict()
    template_name = 'cekin/tambah_gambar.html'
    data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)