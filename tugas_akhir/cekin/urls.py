from django.urls import path

from . import views

app_name = 'cekin'
urlpatterns = [
    path('', views.index, name='index'),
    path('img/',views.imgGerbang, name='image_gerbang'),
    path('tambah/',views.tambahImage, name='image_tambah'),
    path('deteksi/',views.deteksiImage, name='image_deteksi'),
    path('simpan/',views.simpanImage, name='image_simpan'),
    path('video/',views.videoGerbangMasuk, name='video_gerbang'),
    path('camera/', views.aturCamera, name='atur_camera'),
    path('set/', views.setImage, name='set_image'),
    path('<int:pk>/lihat/',views.lihatGambar, name='lihat_gambar'),
] 