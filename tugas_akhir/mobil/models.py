from django.db import models

# Create your models here.
class Kendaraan(models.Model):
    plat_nomor = models.CharField(max_length=200)
    cek_in = models.CharField(max_length=200)
    cek_out = models.CharField(max_length=200)  
    status_kendaraan = models.BooleanField(default=True)
    status_parkir = models.BooleanField(default=False)
    gambar = models.CharField(max_length=200 ,default="", editable=False)  
    gambar_ori = models.CharField(max_length=200 ,default="", editable=False)  
    