from django.urls import path

from . import views

app_name = 'mobil'
urlpatterns = [
    path('', views.index, name='index'),
    path('create/', views.create, name='create_kendaraan'),
    path('<int:pk>/edit/', views.edit, name='edit_kendaraan'),
    path('<int:pk>/delete/', views.delete, name='delete_kendaraan'),
    path('<int:pk>/lihat/', views.lihatParkir, name='lihat_parkir'),
] 