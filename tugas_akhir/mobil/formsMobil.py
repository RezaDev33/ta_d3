from django import forms
from .models import Kendaraan

class KendaraanForm(forms.ModelForm):
    class Meta:
        model = Kendaraan
        fields = ('plat_nomor', 'cek_in', 'cek_out', )