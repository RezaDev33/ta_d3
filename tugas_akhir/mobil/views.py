from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required

from .models import Kendaraan
from parkir.models import Parkir
from .formsMobil import KendaraanForm
from paket.reza import ImageProses
import os

# Create your views here.

@login_required
def index(request):
    kendaraan = Kendaraan.objects.raw('SELECT * FROM `mobil_kendaraan` LEFT JOIN parkir_parkir ON mobil_kendaraan.id = parkir_parkir.id_kendaraan_id LEFT JOIN parkir_posisi ON parkir_parkir.id_posisi_id = parkir_posisi.id')
    return render(request,'mobil/index.html',{'kendaraan':kendaraan})

@login_required
def save_kendaraan_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            kendaraan = Kendaraan.objects.raw('SELECT * FROM `mobil_kendaraan` LEFT JOIN parkir_parkir ON mobil_kendaraan.id = parkir_parkir.id_kendaraan_id LEFT JOIN parkir_posisi ON parkir_parkir.id_posisi_id = parkir_posisi.id')
            data['html_kendaraan_list'] = render_to_string('mobil/list.html', {
                'kendaraan': kendaraan
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
def create(request):
    if request.method == 'POST':
        form = KendaraanForm(request.POST)
    else:
        form = KendaraanForm()
    return save_kendaraan_form(request, form, 'mobil/create.html')

@login_required
def edit(request, pk):
    mobil = get_object_or_404(Kendaraan, pk=pk)
    if request.method == 'POST':
        form = KendaraanForm(request.POST, instance=mobil)
    else:
        form = KendaraanForm(instance=mobil)
    return save_kendaraan_form(request, form, 'mobil/edit.html')

@login_required
def delete(request, pk):
    mobil = get_object_or_404(Kendaraan, pk=pk)
    data = dict()
    if request.method == 'POST':
        os.remove(mobil.gambar)
        os.remove(mobil.gambar_ori)
        parkir = Parkir.objects.filter(id_kendaraan=mobil.id)
        if parkir: # jika mobil sudah terparkir
            os.remove(parkir[0].gambar_parkir)
        parkir.delete()
        mobil.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        kendaraan = Kendaraan.objects.raw('SELECT * FROM `mobil_kendaraan` LEFT JOIN parkir_parkir ON mobil_kendaraan.id = parkir_parkir.id_kendaraan_id LEFT JOIN parkir_posisi ON parkir_parkir.id_posisi_id = parkir_posisi.id')
        data['html_kendaraan_list'] = render_to_string('mobil/list.html', {
            'kendaraan': kendaraan
        })
    else:
        context = {'kendaraan': mobil}
        data['html_form'] = render_to_string('mobil/delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)

@login_required
def lihatParkir(request,pk):
    data = dict()
    image = ImageProses()
    parkir = get_object_or_404(Parkir, id_kendaraan=pk)
    image.setImage(parkir.gambar_parkir)
    gambar_parkir = image.get_image()
    data['form_is_valid'] = True
    data['html_kendaraan_list'] = render_to_string('mobil/lihat_parkir.html', {
                'gambar_parkir': gambar_parkir
            })
    context = {'gambar_parkir':gambar_parkir}
    data['html_form'] = render_to_string('mobil/lihat_parkir.html', context, request=request)
    return JsonResponse(data)
