from django.db import models

# Create your models here.
class Camera(models.Model):
    FileVideo = 0
    WebCamera = 1
    IpCamera = 3
    Camera_TYPES = (
        (FileVideo, 'File Video'),
        (WebCamera, 'Web Camera'),
        (IpCamera, 'IP camera'),
    )
    ip_camera = models.CharField(max_length=200)
    file_camera = models.CharField(max_length=200, default= "")
    web_camera = models.IntegerField()
    status_active = models.PositiveSmallIntegerField(choices=Camera_TYPES)