from django.urls import path

from . import views

app_name = 'dashboard'
urlpatterns = [
    path('', views.index, name='index'),
    path('video_ori/', views.videoOri, name='video_ori'),
    path('video_tracking/', views.videoTracking, name='video_tracking'),
    path('camera/', views.aturCamera, name='atur_camera'),
] 