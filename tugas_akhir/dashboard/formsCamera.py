from django import forms
from .models import Camera

class CameraForm(forms.ModelForm):
    class Meta:
        model = Camera
        fields = ('ip_camera', 'web_camera', 'file_camera', 'status_active', )