from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse,StreamingHttpResponse,HttpResponseServerError,JsonResponse
from django.views.decorators import gzip
from django.db.models import Prefetch
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required


from mobil.models import Kendaraan
from parkir.models import Posisi
from paket.reza import VideoProses, IpCamera
from .models import Camera
from .formsCamera import CameraForm

import os
import json

# Create your views here.
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

# loop frame video
def gen_ori(camera,status):
    mobil = Kendaraan.objects.filter(status_kendaraan=True,status_parkir=False)# kenapa ada ini biar waktu eksekusi sama
    camera.setGambarMobil(mobil)        
    if status == True:
        while True:
            frame ,frame_track = camera.get_frame()
            yield(b'--frame\r\n'
            b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
    else:
        frame = camera.get_frameFail()
        yield(b'--frame\r\n'
        b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

def gen_tracking(camera,status):
    titik = Posisi.objects.raw("SELECT parkir_posisi.id, parkir_posisi.nama, parkir_posisi.koordinat, mobil_kendaraan.plat_nomor FROM parkir_posisi LEFT JOIN parkir_parkir ON parkir_posisi.id = parkir_parkir.id_posisi_id LEFT JOIN mobil_kendaraan ON parkir_parkir.id_kendaraan_id = mobil_kendaraan.id")
    mobil = Kendaraan.objects.filter(status_kendaraan=True,status_parkir=False)
    camera.setSize((768,432))
    camera.setGambarMobil(mobil)
    camera.setTitikParkir(titik)
    if status == True:
        while True:
            frame, frame_track = camera.get_frame()
            # frame, frame_track = camera.get_frame_rstp()
            yield(b'--frame\r\n'
            b'Content-Type: image/jpeg\r\n\r\n' + frame_track + b'\r\n\r\n')            
    else:
        frame = camera.get_frameFail()
        yield(b'--frame\r\n'
        b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

# view untuk demo video
@gzip.gzip_page
def videoOri(request):
    global PROJECT_ROOT
    kamera = Camera.objects.get(id=1)# id 1 untuk tempat parkir
    if kamera.status_active == 0:
        vid = VideoProses() 
        cam = vid.setVideo(PROJECT_ROOT+kamera.file_camera)
    elif kamera.status_active == 1:
        vid = VideoProses()
        cam = vid.setVideo(kamera.web_camera)
    else:
        vid = IpCamera()
        cam = vid.setUrl(kamera.ip_camera)
        # cam = vid setUrlRstp(kamera.ip_camera)
    try:
        return StreamingHttpResponse(gen_ori(vid,cam),content_type="multipart/x-mixed-replace;boundary=frame")
    except HttpResponseServerError as e:
        return HttpResponse(e)

@gzip.gzip_page
def videoTracking(request):
    global PROJECT_ROOT
    kamera = Camera.objects.get(id=1)
    if kamera.status_active == 0:
        vid = VideoProses() 
        cam = vid.setVideo(PROJECT_ROOT+kamera.file_camera)
    elif kamera.status_active == 1:
        vid = VideoProses()
        cam = vid.setVideo(kamera.web_camera)
    else:
        vid = IpCamera()
        # cam = vid.setUrl(kamera.ip_camera)
        cam = vid.setUrlRstp(kamera.ip_camera)
    try:
        return StreamingHttpResponse(gen_tracking(vid,cam),content_type="multipart/x-mixed-replace;boundary=frame")
    except HttpResponseServerError as e:
        return HttpResponse(e)

@login_required
def index(request):
    kendaraan = Kendaraan.objects.all().count()
    return render(request, 'dashboard/index.html',{'kendaraan':kendaraan})

@login_required
def aturCamera(request):
    cam = get_object_or_404(Camera, pk=1)
    if request.method == 'POST':
        form = CameraForm(request.POST, instance=cam)
    else:
        form = CameraForm(instance=cam)
    return save_camera_form(request, form, 'dashboard/camera.html')

@login_required
def save_camera_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)