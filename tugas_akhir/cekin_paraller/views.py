from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse,JsonResponse
from django.template.loader import render_to_string
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import login_required

from mobil.models import Kendaraan
from paket.reza import ImageProses
from datetime import datetime
import os
import base64

# Create your views here.
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

@login_required
def index(request):
    kendaraan = Kendaraan.objects.all().order_by('-id')
    return render(request, 'cekin_paraller/index.html',{'kendaraan':kendaraan})

@login_required
def imgGerbang(request):
    global PROJECT_ROOT
    data = dict()
    image = ImageProses()
    if request.method == 'POST':
        try:
            gambar_awal = request.POST.get("b64awal")
            gambar_mobil = request.POST.get("b64mobil")
            gambar_mobil_paraller = request.POST.get("b64mobilParaller")
            _, imgawal = gambar_awal.split(';base64,')
            _, imgmobil = gambar_mobil.split(';base64,')
            _, imgmobil_paraller = gambar_mobil_paraller.split(';base64,')
            with open(PROJECT_ROOT+"/static/imageParaller/imageAwal.jpg", "wb") as fh:
                fh.write(base64.b64decode(imgawal))
            with open(PROJECT_ROOT+"/static/imageParaller/imageMobil.jpg", "wb") as fh:
                fh.write(base64.b64decode(imgmobil))
            with open(PROJECT_ROOT+"/static/imageParaller/imageMobilParaller.jpg", "wb") as fh:
                fh.write(base64.b64decode(imgmobil_paraller))
            image.setImage(PROJECT_ROOT+"/static/imageParaller/imageMobilParaller.jpg")
            data['form_is_valid'] = True
        except:
            data['html_form'] = render_to_string('cekin_paraller/tambah_gambar.html', request=request)
            data['form_is_valid'] = False
    else:
        data['form_is_valid'] = False
    image.setSize((500,500))
    img = image.get_image()
    data['img'] = img
    return JsonResponse(data)

@login_required
def deteksiImage(request):
    global PROJECT_ROOT
    data = dict()
    image = ImageProses()
    x = int(request.POST.get("x"))
    y = int(request.POST.get("y"))
    w = int(request.POST.get("width"))
    h = int(request.POST.get("height"))
    image.setImage(PROJECT_ROOT+"/static/imageParaller/imageMobilParaller.jpg") 
    image.setSize((500,500))
    text = image.get_ocrPlat(x,y,w,h)
    data['text'] = text
    return JsonResponse(data)

@login_required
def simpanImage(request):
    global PROJECT_ROOT
    data = dict()
    imgawal = PROJECT_ROOT+"/static/imageParaller/imageAwal.jpg"
    image = ImageProses()
    image.setImage(PROJECT_ROOT+"/static/imageParaller/imageMobil.jpg") 
    mobil_asli = PROJECT_ROOT+"/static/imageParaller/imageMobilParaller.jpg"
    image.setSize((768,432))
    text = request.POST.get("text")
    cek_mobil = Kendaraan.objects.filter(plat_nomor = text)
    if len(cek_mobil)==0:
        data['text'] = text
        today = datetime.today()
        date = today.strftime("%d.%m.%Y %H:%M")
        img_ori ,img_dir = image.get_sift_paraller(imgawal,mobil_asli)
        mobil = Kendaraan(plat_nomor = text,cek_in = date,status_kendaraan = True,status_parkir = False,gambar=img_dir,gambar_ori = img_ori)
        mobil.save()
        data['cek_mobil'] = True
    else:
        data['cek_mobil'] = False        
    kendaraan = Kendaraan.objects.all().order_by('-id')
    data['html_list'] = render_to_string('cekin_paraller/list.html',{
        'kendaraan':kendaraan
    })        
    return JsonResponse(data)

@login_required
def lihatGambar(request,pk):
    data = dict()
    image = ImageProses()
    mobil = get_object_or_404(Kendaraan, pk=pk)
    image.setImage(mobil.gambar_ori)
    gambar_mobil = image.get_image()
    data['form_is_valid'] = True
    data['html_kendaraan_list'] = render_to_string('cekin_paraller/lihat_gambar.html', {
                'gambar_mobil': gambar_mobil
            })
    context = {'gambar_mobil':gambar_mobil}
    data['html_form'] = render_to_string('cekin_paraller/lihat_gambar.html', context, request=request)
    return JsonResponse(data)

# hanya template form
@login_required
def tambahImage(request): 
    data = dict()
    template_name = 'cekin_paraller/tambah_gambar.html'
    data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)

