from django.urls import path

from . import views

app_name = 'cekin_paraller'
urlpatterns = [
    path('', views.index, name='index'),
    path('img/',views.imgGerbang, name='image_gerbang'),
    path('tambah/',views.tambahImage, name='image_tambah'),
    path('deteksi/',views.deteksiImage, name='image_deteksi'),
    path('simpan/',views.simpanImage, name='image_simpan'),
    path('<int:pk>/lihat/',views.lihatGambar, name='lihat_gambar'),
] 