from django.urls import path

from . import views

app_name = 'user_manage'
urlpatterns = [
    path('',views.index, name = 'index'),
] 